Name:           mousetweaks
Version:        3.32.0
Release:        2
Summary:        Accessibility enhancements for the mouse
License:        GPL-3.0-or-later
URL:            http://live.gnome.org/Mousetweaks/Home
Source0:        http://download.gnome.org/sources/mousetweaks/3.32/%{name}-%{version}.tar.xz
Patch9000:      bugfix-Fix-invalid-UTF-8.patch

BuildRequires:  gcc gettext gnome-doc-utils pkgconfig GConf2-devel gtk3-devel >= 3.0.0 libXcursor-devel
BuildRequires:  libXtst-devel libXfixes-devel libglade2-devel gsettings-desktop-schemas-devel intltool
Requires(pre):  GConf2

%description
Mousetweaks is a daemon that provides various mouse features for the GNOME desktop.
It depends on the Assistive Technology Service Provider Interface (AT-SPI).

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --disable-scrollkeeper
%make_build

%install
%make_install
%find_lang mousetweaks --with-gnome

%pre
%gconf_schema_obsolete mouseweaks pointer-capture-applet

%postun
if [ $1 -eq 0 ]; then
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :
fi

%posttrans
glib-compile-schemas %{_datadir}/glib-2.0/schemas &>/dev/null || :

%files -f mousetweaks.lang
%doc COPYING README NEWS
%{_datadir}/GConf/gsettings/mousetweaks.convert
%{_datadir}/glib-2.0/schemas/{org.gnome.mousetweaks.enums.xml,org.gnome.mousetweaks.gschema.xml}
%{_bindir}/mousetweaks
%{_datadir}/mousetweaks

%files help
%doc %{_mandir}/man1/*

%changelog
* Tue Aug 20 2024 Wei Zhang <zhangwei@cqsoftware.com.cn> - 3.32.0-2
- Replaced declaration of help subpackage with the 'package_help' macro

* Wed May 10 2023 Ge Wang <wang__ge@126.com> - 3.32.0-1
- Update to version 3.32.0

* Wed Apr 27 2022 wangyueliang <wangyueliang@kylinos.cn> - 3.12.0-13
- Improve the project according to the requirements of compliance improvement.

* Wed Jun 02 2021 zhaoyao <zhaoyao32@huawei.com> - 3.12.0-12
- fixs faileds: /bin/sh: gcc: command not found.  

* Wed Dec 11 2019 shijian <shijian16@huawei.com> - 3.12.0-11
- Package init
